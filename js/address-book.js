/* address-book.js
    this is where you will add your JavaScript to complete Lab 5
*/
$(document).ready(function() {
    make_copies();
    populate_data();
    render();
})

/*$(function() {
    sortObjArray(Employees.entries,'last');
    render();
}*/

function make_copies() {
    lab5.array = [];
    lab5.$template = $('.address-book')
    var element = lab5.$template.html();
    for (var i = 0; i < Employees.entries.length; i++) {
        var $element = $(element)
        $element.removeClass('template')
        lab5.array.push($element);
    }
}

function populate_data() {
    for (var i = 0; i < Employees.entries.length; i++) {
        lab5.array[i].find('h2.name').text(Employees.entries[i].first + ' ' + Employees.entries[i].last);
        lab5.array[i].find('p.title').text(Employees.entries[i].title);
        lab5.array[i].find('span.dept').text(Employees.entries[i].dept);
        lab5.array[i].find('img.pic').attr('src',Employees.entries[i].pic);
    }
}

function render() {
    for (var i = 0; i < Employees.entries.length; i++) {
        lab5.$template.append(lab5.array[i]);
    }
}

/* sortObjArray()
    sorts an array of objects by a given property name
    the property values are compared using standard 
    operators, so this will work for string, numeric,
    boolean, or date values

    objArray        array of objects to sort
    propName        property name to sort by

    returns undefined (array is sorted in place)
*/
function sortObjArray(objArray, propName) {
    if (!objArray.sort)
        throw new Error('The objArray parameter does not seem to be an array (no sort method)');

    //sort the array supplying a custom compare function
    objArray.sort(function(a,b) {
        
        //note: this compares only one property of the objects
        //see the optional step where you can add support for 
        //a secondary sort key (i.e., sort by another property)
        //if the first property values are equal
        if (a[propName] < b[propName])
            return -1;
        else if (a[propName] === b[propName])
            return 0;
        else
            return 1;
    });
} //sortObjArray()

